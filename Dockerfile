FROM eclipse-temurin:18.0.2_9-jre-focal

WORKDIR /app

COPY target/*jar /app/api.jar

EXPOSE 8080

CMD ["java", "-jar", "api.jar"]